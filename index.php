<?php
require './acess.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">
    <title>Document</title>
</head>

<body>
    <div class="container">

        <div class="form">
            <div class="form--title">
                <h1>Câu trắc nghiệm nhanh</h1>
            </div>
            <form method="POST">
                <div class="form--data">
                    <div class="form--data__item">
                        <?php
                        foreach ($list_question as $questions) {
                        ?>
                            <div class="item--question">
                                <h4><?php echo $questions[5] . ". " . $questions[0] ?></h4>
                            </div>
                            <div class="item--answer">
                                <input type="radio" name="<?php echo $questions[5] ?>" value="1">
                                <label for=""> <?php echo $questions[1] ?></label>
                                <input type="radio" name="<?php echo $questions[5] ?>" value="2">
                                <label for=""> <?php echo $questions[2] ?></label>
                                <input type="radio" name="<?php echo $questions[5] ?>" value="3">
                                <label for=""> <?php echo $questions[3] ?></label>
                                <input type="radio" name="<?php echo $questions[5] ?>" value="4">
                                <label for=""> <?php echo $questions[4] ?></label>

                            </div>
                        <?php
                        }
                        ?>
                    </div>

                </div>
                <div class="mid--data" hidden>
                    <div class="form--data__item">
                        <?php
                        foreach ($question_next as $key) {
                        ?>
                            <div class="item--question">
                                <h4> <?php echo $key[5] . ". " . $key[0] ?></h4>
                            </div>
                            <div class="item--answer">
                                <input type="radio" name="<?php echo $key[5] ?>" value="1">
                                <label for=""> <?php echo $key[1] ?></label>
                                <input type="radio" name="<?php echo $key[5] ?>" value="2">
                                <label for=""> <?php echo $key[2] ?></label>
                                <input type="radio" name="<?php echo $key[5] ?>" value="3">
                                <label for=""> <?php echo $key[3] ?></label>
                                <input type="radio" name="<?php echo $key[5] ?>" value="4">
                                <label for=""> <?php echo $key[4] ?></label>

                            </div>
                        <?php
                        }
                        ?>
                    </div>


                </div>
                <div class="Select">
                    <input class="nextPage" type="text" value="Next">
                    <input class="backPage" type="text" value="Back">
                    <!-- <button class="nextPage">Next</button>
                    <button class="backPage">Back</button> -->
                    <input class="submit" type="submit" name="submit">
                </div>

            </form>
            <?php
            if(isset($_POST['submit'])){
                var_dump($_POST);
            }
            
            ?>

        </div>

        <!-- </form> -->
    </div>
    <script src="./jquery-3.6.1.min.js"></script>
    <script src="./nextPage.js"></script>
</body>

</html>